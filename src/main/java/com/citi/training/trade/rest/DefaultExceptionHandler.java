package com.citi.training.trade.rest;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.citi.training.trade.exception.TradeNotFoundException;
import com.citi.training.trade.rest.DefaultExceptionHandler;

@ControllerAdvice
@Priority(20)
public class DefaultExceptionHandler {

private static final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);
    
    @ExceptionHandler(value = {TradeNotFoundException.class})
    public ResponseEntity<Object> TradeNotFoundExceptionHandler(
            HttpServletRequest request, TradeNotFoundException ex404) {
        logger.warn(ex404.toString());
        return new ResponseEntity<>("{\"message\":\""+ex404.getMessage()+"\"}", HttpStatus.NOT_FOUND);
    }
}
