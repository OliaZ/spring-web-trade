package com.citi.training.trade.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.citi.training.trade.model.Trade;
import com.citi.training.trade.service.TradeService;

@RestController
@RequestMapping("${com.citi.training.trade.base_path}")
public class TradeController {
    
    private static final Logger logger = LoggerFactory.getLogger(TradeController.class);    

    @Autowired
    TradeService tradeService;

    @RequestMapping(value="", method = RequestMethod.GET, 
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Trade> findAll() {
        
        logger.debug("TRADE LIST: " + tradeService.findAll());
        
        return tradeService.findAll();

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, 
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Trade findById(@PathVariable int id) {
        
        logger.debug("id " + id + " refers " + tradeService.findById(id)+ " trade" );
        
        return tradeService.findById(id);
    }

    @RequestMapping(method = RequestMethod.POST, 
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Trade> create(@RequestBody Trade trade) {
        trade.setId(tradeService.create(trade));
        return new ResponseEntity<Trade>(trade, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        tradeService.deleteById(id);
    }
}
