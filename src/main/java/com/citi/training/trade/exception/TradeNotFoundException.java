package com.citi.training.trade.exception;

@SuppressWarnings("serial")
public class TradeNotFoundException extends RuntimeException {

    public TradeNotFoundException(String msg) {
        super(msg);
    }

}
