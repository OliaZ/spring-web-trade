package com.citi.training.trade.model;

import org.junit.Test;

import com.citi.training.trade.model.Trade;

public class TradeTest {

    @Test
    public void test_Trade_fullConstructor() {
        Trade testTrade = new Trade(0, "AAPL", 9.99, 6);
        assert(testTrade.getId() == 0);
        assert(testTrade.getStock().equals("AAPL"));
        assert(testTrade.getPrice() == 9.99);
        assert(testTrade.getVolume() == 6);
    }

    @Test
    public void test_Trade_setters() {
        Trade testTrade = new Trade(0, "AAPL", 9.99, 6);
        testTrade.setId(1);
        testTrade.setStock("APPL");
        testTrade.setPrice(10.00);
        testTrade.setVolume(20);

        assert(testTrade.getId() == 1);
        assert(testTrade.getStock().equals("APPL"));
        assert(testTrade.getPrice() == 10.00);
        assert(testTrade.getVolume() == 20);
    }

    @Test
    public void test_Trade_toString() {
        Trade testTrade = new Trade(0, "AAPL", 9.99, 6);

        assert(testTrade.toString() != null);
        assert(testTrade.toString().contains(testTrade.getStock()));
    }
}
   
