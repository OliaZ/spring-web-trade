package com.citi.training.trade.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trade.dao.TradeDao;
import com.citi.training.trade.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeServiceTest {
    

    @Autowired
    private TradeService tradeService;

    @MockBean
    private TradeDao mockTradeDao;

    @Test
    public void test_createRun() {
        int id = 3;
        Trade testTrade = new Trade(87, "GOOGL", 102.98, 114);
        
        when(mockTradeDao.create(any(Trade.class))).thenReturn(id);
        int createdId = tradeService.create(testTrade);
        
        verify(mockTradeDao).create(testTrade);
        //verify(mockTradeDao).create(any(Trade.class));
        assertEquals(id, createdId);
    }
    
    @Test
    public void test_deleteByIdRun() {
        tradeService.deleteById(75);
        
        verify(mockTradeDao).deleteById(75);
    }
    
    @Test
    public void test_findById() {
        int id = 3;
        Trade testTrade = new Trade(id, "GOOGL", 110.45, 877);
        
        when(mockTradeDao.findById(id)).thenReturn(testTrade);
        
        Trade returnedProd = tradeService.findById(id);
        
        verify(mockTradeDao).findById(3);
        assertEquals(testTrade, returnedProd);
    }
    
    @Test
    public void test_findAll() {
        List<Trade> testTradeList = new ArrayList<Trade>();
        testTradeList.add(new Trade (67, "MSFT", 35.76, 67));
        
        when(mockTradeDao.findAll()).thenReturn(testTradeList);
        
        List<Trade> returnedTradeList = tradeService.findAll();
        verify(mockTradeDao).findAll();
        assertEquals(testTradeList, returnedTradeList);
        
    }    

}
